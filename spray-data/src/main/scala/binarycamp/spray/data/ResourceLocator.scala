package binarycamp.spray.data

import binarycamp.spray.data.ResourceLocator._
import shapeless._
import spray.http.Uri
import spray.http.Uri.Path
import spray.http.Uri.Path.Empty
import scala.annotation.tailrec
import scala.language.implicitConversions

trait ReferenceWriter[R] {
  def write(reference: R): Uri
}

object ReferenceWriter {
  implicit object UriReferenceWriter extends ReferenceWriter[Uri] {
    override def write(uri: Uri): Uri = uri
  }
}

object ResourceLocator {
  trait BaseUriProvider {
    def baseUri: Uri
  }

  def apply(implicit provider: BaseUriProvider): ResourceLocator0 = apply(provider.baseUri)

  def apply(baseUri: String): ResourceLocator0 = apply(Uri(baseUri))

  def apply(baseUri: Uri): ResourceLocator0 = new ResourceLocator0 {
    require(baseUri.isAbsolute)
    require(baseUri.query.isEmpty)
    require(baseUri.fragment.isEmpty)

    override def rawRead(uri: Uri): Matching[HNil] = {
      @tailrec
      def diff(base: Path, path: Path): Option[Path] = (base, path) match {
        case (Empty, _)                       ⇒ Some(path)
        case (p1, p2) if (p1.head == p2.head) ⇒ diff(p1.tail, p2.tail)
        case _                                ⇒ None
      }

      if (baseUri.scheme == uri.scheme && baseUri.authority == uri.authority) {
        diff(baseUri.path, uri.path) match {
          case Some(rest) ⇒ Matched(rest, HNil)
          case None       ⇒ Unmatched
        }
      } else Unmatched
    }

    override def write(reference: HNil): Uri = baseUri
  }

  sealed trait Mapper[T, U] {
    def from(t: T): U
    def to(u: U): T
  }

  object Mapper {
    implicit def forHList[T <: HList, T2 <: HList, U](implicit g: Generic.Aux[U, T2], ev1: T2 =:= T,
                                                      ev2: T =:= T2): Mapper[T, U] =
      new Mapper[T, U] {
        override def from(t: T): U = g.from(t)
        override def to(u: U): T = g.to(u)
      }
  }

  sealed trait Matching[+L] {
    def map[R](f: L ⇒ R): Matching[R]
    def andThen[R](f: (Path, L) ⇒ Matching[R]): Matching[R]
  }

  case class Matched[L](rest: Path, extractions: L) extends Matching[L] {
    override def map[R](f: L ⇒ R): Matching[R] = Matched(rest, f(extractions))
    override def andThen[R](f: (Path, L) ⇒ Matching[R]): Matching[R] = f(rest, extractions)
  }

  case object Unmatched extends Matching[Nothing] {
    override def map[R](f: (Nothing) ⇒ R): Matching[R] = Unmatched
    override def andThen[R](f: (Path, Nothing) ⇒ Matching[R]): Matching[R] = Unmatched
  }

  trait PathMatcher[L <: HList] { self ⇒
    def read(path: Path): Matching[L]
    def write(value: L): Path
    def as[R](implicit m: Mapper[L, R]): PathMatcher[R :: HNil] =
      new PathMatcher[R :: HNil] {
        override def read(path: Path): Matching[R :: HNil] = self.read(path).map(m.from(_) :: HNil)
        override def write(value: R :: HNil): Path = self.write(m.to(value.head))
      }
  }

  type PathMatcher0 = PathMatcher[HNil]

  object PathMatcher {
    implicit def fromString(const: String) =
      new PathMatcher0 {
        override def read(path: Path): Matching[HNil] =
          path match {
            case Path.Segment(`const`, tail) ⇒ Matched(tail, HNil)
            case _                           ⇒ Unmatched
          }

        override def write(value: HNil): Path = Path(const)
      }

    object Slash extends PathMatcher0 {
      override def read(path: Path): Matching[HNil] =
        path match {
          case Path.Slash(tail) ⇒ Matched(tail, HNil)
          case _                ⇒ Unmatched
        }

      override def write(value: HNil): Path = Path.SingleSlash
    }

    object Segment extends PathMatcher[String :: HNil] {
      override def read(path: Path): Matching[String :: HNil] =
        path match {
          case Path.Segment(head, tail) ⇒ Matched(tail, head :: HNil)
          case _                        ⇒ Unmatched
        }

      override def write(value: String :: HNil): Path = Path(value.head)
    }
  }

  trait LocatorMagnet[T] {
    type Out
    def apply(locator: ResourceLocator[T]): Out
  }

  object LocatorMagnet extends LowPriorityLocatorMagnetImplicits1 {
    implicit def forString[L](const: String) = forL0[L](PathMatcher.fromString(const))

    implicit def forL0[L](pm: PathMatcher0) =
      new LocatorMagnet[L] {
        type Out = ResourceLocator[L]
        def apply(underlying: ResourceLocator[L]): Out =
          underlying.transform(_.andThen((rest, l) ⇒ pm.read(rest).map(_ ⇒ l)), (_, pm.write(HNil)))
      }
  }

  trait LowPriorityLocatorMagnetImplicits1 extends LowPriorityLocatorMagnetImplicits2 {
    implicit def forHList[L <: HList, M <: HList](pm: PathMatcher[M])(implicit p: BiPrepend[L, M]) =
      new LocatorMagnet[L] {
        type Out = ResourceLocator[p.Out]
        def apply(underlying: ResourceLocator[L]): Out =
          underlying.transform(_.andThen((rest, l) ⇒ pm.read(rest).map(p.prepend(l, _))), { out ⇒
            val (l, m) = p.split(out)
            (l, pm.write(m))
          })
      }
  }

  trait LowPriorityLocatorMagnetImplicits2 {
    implicit def forL[L, M <: HList](pm: PathMatcher[M])(implicit p: BiPrepend[L :: HNil, M]) =
      new LocatorMagnet[L] {
        type Out = ResourceLocator[p.Out]
        def apply(underlying: ResourceLocator[L]): Out =
          underlying.transform(_.andThen((rest, l) ⇒ pm.read(rest).map(p.prepend(l :: HNil, _))), { out ⇒
            val (l, m) = p.split(out)
            (l.head, pm.write(m))
          })
      }
  }
}

sealed trait ResourceLocator[R] extends ReferenceWriter[R] { self ⇒
  def read(uri: Uri): Option[R] = rawRead(uri) match {
    case Matched(Path.Empty, extractions) ⇒ Some(extractions)
    case _                                ⇒ None
  }

  def write(value: R): Uri

  private[data] def rawRead(uri: Uri): Matching[R]

  def /(magnet: LocatorMagnet[R]): magnet.Out = this ~ PathMatcher.Slash ~ magnet

  def ~(magnet: LocatorMagnet[R]): magnet.Out = magnet(this)

  def as[S](from: R ⇒ S, to: S ⇒ R): ResourceLocator[S] =
    new ResourceLocator[S] {
      override def write(ref: S): Uri = self.write(to(ref))
      override def rawRead(uri: Uri): Matching[S] = self.rawRead(uri).map(from)
    }

  def as[S](implicit m: Mapper[R, S]): ResourceLocator[S] = as[S](m.from _, m.to _)

  def transform[S](r: Matching[R] ⇒ Matching[S], w: S ⇒ (R, Path)): ResourceLocator[S] =
    new ResourceLocator[S] {
      override def write(value: S): Uri = {
        val (l, suffix) = w(value)
        val uri = self.write(l)
        uri.withPath(path = uri.path ++ suffix)
      }

      override def rawRead(uri: Uri): Matching[S] = r(self.rawRead(uri))
    }
}
