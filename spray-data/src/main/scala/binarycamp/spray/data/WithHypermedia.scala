package binarycamp.spray.data

import scala.language.implicitConversions

case class WithHypermedia[T](entity: T, links: Option[Seq[Link]])

object WithHypermedia {
  def apply[T](entity: T): WithHypermedia[T] = new WithHypermedia[T](entity, None)
}

trait HypermediaPimps {
  implicit def pimpPage[T](page: Page[T]): PimpedPage[T] = new PimpedPage(page)
}

final class PimpedPage[T](val page: Page[T]) extends AnyVal {
  def asCollectionResource(linkProvider: LinkProvider[Page[T]]): WithHypermedia[Page[T]] =
    WithHypermedia[Page[T]](page, linkProvider(page))
}
