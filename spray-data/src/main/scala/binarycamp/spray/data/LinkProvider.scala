package binarycamp.spray.data

import binarycamp.spray.pimp._
import scala.language.implicitConversions

trait LinkProvider[T] extends (T ⇒ Option[Seq[Link]])

object LinkProvider extends PageLinkProviderImplicits {
  def apply[T](f: T ⇒ Option[Seq[Link]]): LinkProvider[T] = new LinkProvider[T] {
    override def apply(t: T): Option[Seq[Link]] = f(t)
  }
}

trait PageLinkProviderImplicits {
  implicit def fromReference[R, T](reference: R)(implicit w: ReferenceWriter[R]): LinkProvider[Page[T]] =
    LinkProvider { page ⇒
      val href = w.write(reference)
      Some(Link(href.withQueryUpdated(PageRequest(page))) +:
        PageRequest.prev(page).map(request ⇒ Link(Link.Prev, href.withQueryUpdated(request))).toList ++:
        PageRequest.next(page).map(request ⇒ Link(Link.Next, href.withQueryUpdated(request))).toList)
    }
}
