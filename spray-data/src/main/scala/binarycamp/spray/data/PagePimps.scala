package binarycamp.spray.data

import scala.language.implicitConversions

trait PagePimps {
  implicit def seqToPagePimpedSeq[T](seq: Seq[T]): PimpedSeq[T] = new PimpedSeq[T](seq)
}
