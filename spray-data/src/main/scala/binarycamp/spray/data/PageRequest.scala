package binarycamp.spray.data

import binarycamp.spray.common.ParamMapWriter

case class PageRequest(index: Int, size: Option[Int], sort: Option[Sort], filter: Option[Filter]) {
  require(index >= 0, "Page index must be a non-negative integer")
  require(size.isEmpty || size.get > 0, "Page size must be a positive integer")
}

object PageRequest {
  // format: OFF
  val IndexParam  = "page"
  val SizeParam   = "size"
  val SortParam   = "sort"
  val FilterParam = "filter"

  val AscParam    = "asc"
  val DescParam   = "desc"
  // format: ON

  def apply(index: Int, size: Int, sort: Option[Sort], filter: Option[Filter]): PageRequest =
    PageRequest(index, Some(size), sort, filter)

  def apply[T](page: Page[T]): PageRequest = apply(page.index, page.size, page.sort, page.filter)

  def prev[T](page: Page[T]): Option[PageRequest] =
    if (page.hasPrevious) Some(apply(page.index - 1, page.size, page.sort, page.filter)) else None

  def next[T](page: Page[T]): Option[PageRequest] =
    if (page.hasNext) Some(apply(page.index + 1, page.size, page.sort, page.filter)) else None

  implicit object PageRequestParamMapWriter extends ParamMapWriter[PageRequest] {
    override def apply(request: PageRequest): Map[String, String] =
      Map(ParamMapWriter.field(IndexParam, request.index) ++
        ParamMapWriter.field(SizeParam, request.size) ++
        ParamMapWriter.field(SortParam, request.sort.map(printSort)) ++
        ParamMapWriter.field(FilterParam, request.filter.map(_.expression)): _*)

    private def printSort(sort: Sort): String =
      sort.fields.map {
        case (name, Some(Order.Asc))  ⇒ s"$name:$AscParam"
        case (name, Some(Order.Desc)) ⇒ s"$name:$DescParam"
        case (name, None)             ⇒ name
      }.mkString(",")
  }
}
