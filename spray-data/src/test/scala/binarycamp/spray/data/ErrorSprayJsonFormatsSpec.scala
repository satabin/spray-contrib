package binarycamp.spray.data

import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import spray.http.Uri
import spray.json._

class ErrorSprayJsonFormatsSpec extends FlatSpec with ErrorSprayJsonFormats {
  val errorWithMessagesAndLinks = MarshallableError("code", "message", Some(Seq("message1", "message2")),
    Some(Seq(Link(Uri("http://localhost/errors/code")))))

  val errorWithMessagesAndLinksJson =
    """|{
       |  "@error": {
       |    "@code": "code",
       |    "@message": "message",
       |    "@messages": ["message1", "message2"],
       |    "@links": {
       |      "self": {
       |        "href": "http://localhost/errors/code"
       |      }
       |    }
       |  }
       |}""".stripMargin.parseJson

  "MarshallableErrorJsonFormat" should "convert a MarshallableError with messages and links to a JsObject" in {
    errorWithMessagesAndLinks.toJson(MarshallableErrorJsonFormat) should be(errorWithMessagesAndLinksJson)
  }

  it should "convert a valid error representation with messages and links to a MarshallableError" in {
    errorWithMessagesAndLinksJson.convertTo(MarshallableErrorJsonFormat) should be(errorWithMessagesAndLinks)
  }

  val errorWithLinks = MarshallableError("code", "message", None, Some(Seq(Link(Uri("http://localhost/errors/code")))))
  val errorWithLinksJson =
    """|{
       |  "@error": {
       |    "@code": "code",
       |    "@message": "message",
       |    "@links": {
       |      "self": {
       |        "href": "http://localhost/errors/code"
       |      }
       |    }
       |  }
       |}""".stripMargin.parseJson

  it should "convert a MarshallableError with links to a JsObject" in {
    errorWithLinks.toJson(MarshallableErrorJsonFormat) should be(errorWithLinksJson)
  }

  it should "convert a valid error representation with links to a MarshallableError" in {
    errorWithLinksJson.convertTo(MarshallableErrorJsonFormat) should be(errorWithLinks)
  }

  val errorWithMessages = MarshallableError("code", "message", Some(Seq("message1", "message2")), None)
  val errorWithMessagesJson =
    """|{
       |  "@error": {
       |    "@code": "code",
       |    "@message": "message",
       |    "@messages": ["message1", "message2"]
       |  }
       |}""".stripMargin.parseJson

  it should "convert a MarshallableError with messages to a JsObject" in {
    errorWithMessages.toJson(MarshallableErrorJsonFormat) should be(errorWithMessagesJson)
  }

  it should "convert a valid error representation with messages to a MarshallableError" in {
    errorWithMessagesJson.convertTo(MarshallableErrorJsonFormat) should be(errorWithMessages)
  }

  val error = MarshallableError("code", "message", None, None)
  val errorJson =
    """|{
       |  "@error": {
       |    "@code": "code",
       |    "@message": "message"
       |  }
       |}""".stripMargin.parseJson

  it should "convert a MarshallableError to a JsObject" in {
    error.toJson(MarshallableErrorJsonFormat) should be(errorJson)
  }

  it should "convert a valid error representation to a MarshallableError" in {
    errorJson.convertTo(MarshallableErrorJsonFormat) should be(error)
  }

  it should "produce DeserializationException when converting a non-JsObject" in {
    intercept[DeserializationException] {
      JsString("any").convertTo(MarshallableErrorJsonFormat)
    }
  }

  it should "produce DeserializationException when converting a JsObject with no @error field" in {
    intercept[DeserializationException] {
      JsObject().convertTo(MarshallableErrorJsonFormat)
    }
  }

  it should "produce DeserializationException when converting a JsObject with no @error.@code field" in {
    intercept[DeserializationException] {
      JsObject("@error" -> JsObject("@message" -> JsString("message"))).convertTo(MarshallableErrorJsonFormat)
    }
  }

  it should "produce DeserializationException when converting a JsObject with no @error.@message field" in {
    intercept[DeserializationException] {
      JsObject("@error" -> JsObject("@code" -> JsString("code"))).convertTo(MarshallableErrorJsonFormat)
    }
  }
}
