package binarycamp.spray.examples.data

import binarycamp.spray.common.{ Error, SingletonError }

object Errors {
  case class BookNotFound(id: String) extends Error with SingletonError
  case class DuplicateBook(name: String) extends Error with SingletonError
  case object InvalidPageRequest extends Error with SingletonError
  case object UnknownError extends Error with SingletonError
}
