package binarycamp.spray.examples.oauth2

import binarycamp.spray.login.{ UserInfo, UserService }
import binarycamp.spray.oauth2.server.Client
import binarycamp.spray.oauth2.server.flows.ClientToUserMapper
import scala.concurrent.Future

case class User(username: String, password: String)

object Users {
  val humans = Map("john" -> User("john", "smith"), "joe" -> User("joe", "average"))
  val systems = Map("extsystem" -> User("extsystem", "password"))

  val userService = new UserService[User] {
    override def authenticate(login: String, password: String): Future[Option[UserInfo[User]]] =
      Future.successful(humans.get(login).find(_.password == password).map(UserInfo(login, _)))

    override def findById(id: String): Future[Option[UserInfo[User]]] =
      Future.successful(humans.get(id).orElse(systems.get(id)).map(UserInfo(id, _)))
  }

  val clientToUserMapper = new ClientToUserMapper[User] {
    override def apply(client: Client): Future[Option[UserInfo[User]]] =
      Future.successful(systems.get(client.id).map(UserInfo(client.id, _)))
  }
}
