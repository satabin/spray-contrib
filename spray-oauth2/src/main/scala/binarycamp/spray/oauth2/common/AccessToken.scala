package binarycamp.spray.oauth2.common

case class AccessToken(tokenType: String, token: String, additionalAttributes: Option[Map[String, String]]) {
  require(additionalAttributes.isEmpty || (additionalAttributes.get.keySet & AccessToken.ReservedKeys).isEmpty,
    "AdditionalAttributes cannot contain any of the reserved keys " + AccessToken.ReservedKeys.mkString(" "))
}

object AccessToken {
  val ReservedKeys = Set("access_token", "token_type", "expires_in", "refresh_token", "scope")

  def apply(tokenType: String, token: String): AccessToken = AccessToken(tokenType, token, None)

  def apply(tokenType: String, token: String, additionalAttributes: Map[String, String]): AccessToken =
    AccessToken(tokenType, token, Some(additionalAttributes))
}

object BearerToken {
  val BearerTokenType = "Bearer"

  def apply(token: String): AccessToken = AccessToken(BearerTokenType, token, None)

  def unapply(token: AccessToken): Option[String] =
    if (token.tokenType == BearerTokenType && token.additionalAttributes.isEmpty) Some(token.token) else None
}

object MacToken {
  val MacTokenType = "mac"
  val Key = "mac_key"
  val Algorithm = "mac_algorithm"

  def apply(token: String, key: String, algorithm: String): AccessToken =
    AccessToken(MacTokenType, token, Map(Key -> key, Algorithm -> algorithm))

  def unapply(token: AccessToken): Option[(String, String, String)] =
    for {
      key ← token.additionalAttributes.flatMap(_.get(Key))
      algorithm ← token.additionalAttributes.flatMap(_.get(Algorithm))
      if (token.tokenType == MacTokenType && token.additionalAttributes.exists(_.size == 2))
    } yield (token.token, key, algorithm)
}
