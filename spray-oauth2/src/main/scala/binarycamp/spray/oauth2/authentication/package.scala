package binarycamp.spray.oauth2

import binarycamp.spray.oauth2.common.AccessToken
import scala.concurrent.Future

package object authentication {
  type AccessTokenAuthenticator[U] = AccessToken ⇒ Future[Option[ScopedUser[U]]]
}
