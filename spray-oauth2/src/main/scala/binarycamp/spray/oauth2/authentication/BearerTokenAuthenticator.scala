package binarycamp.spray.oauth2.authentication

import binarycamp.spray.oauth2.authentication.AbstractTokenAuthenticator._
import binarycamp.spray.oauth2.common.{ AccessToken, BearerToken }
import scala.concurrent.ExecutionContext
import spray.http.HttpHeaders.Authorization
import spray.http.OAuth2BearerToken
import spray.routing._
import spray.util._

class BearerTokenAuthenticator[U](authenticator: AccessTokenAuthenticator[U], realm: String)(implicit ec: ExecutionContext)
    extends AbstractTokenAuthenticator[U](authenticator, "Bearer", realm) {

  override def extractAccessToken(ctx: RequestContext): Either[AccessTokenError, AccessToken] =
    List(fromHeader(ctx), fromQueryParameter(ctx), fromForm(ctx)).partition(_.isDefined) match {
      case (Some(token @ BearerToken(_)) :: Nil, _) ⇒ Right(token)
      case (Nil, _)                                 ⇒ Left(AccessTokenMissing)
      case _                                        ⇒ Left(AccessTokenInvalid)
    }

  private def fromHeader(ctx: RequestContext): Option[AccessToken] =
    ctx.request.headers.findByType[Authorization].flatMap {
      case Authorization(OAuth2BearerToken(token)) ⇒ Some(BearerToken(token))
      case _                                       ⇒ None
    }

  private def fromQueryParameter(ctx: RequestContext): Option[AccessToken] =
    ctx.request.uri.query.get("access_token").map(BearerToken(_))

  private def fromForm(ctx: RequestContext): Option[AccessToken] = None
}

object BearerTokenAuthenticator {
  def apply[U](authenticator: AccessTokenAuthenticator[U])(implicit ec: ExecutionContext): BearerTokenAuthenticator[U] =
    apply(authenticator, "Secured Resource")

  def apply[U](authenticator: AccessTokenAuthenticator[U], realm: String)(implicit ec: ExecutionContext): BearerTokenAuthenticator[U] =
    new BearerTokenAuthenticator[U](authenticator, realm)
}
