package binarycamp.spray.oauth2.common

import com.typesafe.config.ConfigFactory

object TestRedis {
  val config = ConfigFactory.load().getObject("test-redis").toConfig
  val host = config.getString("host")
  val port = config.getInt("port")
}
