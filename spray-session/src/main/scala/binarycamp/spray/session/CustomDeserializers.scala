package binarycamp.spray.session

import spray.http._
import spray.httpx.unmarshalling._

trait CustomDeserializers {
  implicit val uriDeserializer = new FromStringDeserializer[Uri] {
    override def apply(value: String): Deserialized[Uri] =
      try Right(Uri(java.net.URLDecoder.decode(value, "UTF-8")))
      catch { case e: IllegalUriException ⇒ Left(MalformedContent(s"'$value' is not a valid Uri", e)) }
  }
}

object CustomDeserializers extends CustomDeserializers
