package binarycamp.spray.common

import binarycamp.spray.common.Errors._
import spray.http.LanguageRange

trait ErrorMarshallingSupport {
  implicit val TestErrorTranslator = new ErrorTranslator[String] {
    override def apply(error: Error, languages: Seq[LanguageRange]): String = error match {
      case Error1 if languages.exists(_.matches(DE)) ⇒ Error1Message_DE
      case Error1                                    ⇒ Error1Message_EN
      case Error2 if languages.exists(_.matches(DE)) ⇒ Error2Message_DE
      case Error2                                    ⇒ Error2Message_EN
      case error                                     ⇒ error.code
    }
  }

  implicit val TestStatusCodeMapper = ErrorToStatusCodeMapper {
    case Error1 ⇒ Error1StatusCode
    case Error2 ⇒ Error2StatusCode
  }

  def result(error: Error): Either[Error, String] = Left(error)
}
