package binarycamp.spray.pimp

import spray.http.Uri.Query

class PimpedQuery(val query: Query) extends AnyVal {
  def +?(kv: (String, Option[String])): Query = if (kv._2.isDefined) query.+:(kv._1 -> kv._2.get) else query
}
