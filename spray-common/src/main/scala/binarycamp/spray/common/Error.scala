package binarycamp.spray.common

import scala.util.matching.Regex.Match

abstract class Error {
  def code: String
  def format(message: String): String = message
  def tags: Seq[ErrorTag] = Nil
  def hasTag(tag: ErrorTag): Boolean = tags.contains(tag)
}

trait SingletonError extends Error with Product {
  final override val code = SingletonError.camelToSnake(productPrefix)
}

private[common] object SingletonError {
  val r1 = "([.][A-Z])([A-Z][a-z])".r
  val r2 = "([a-z])([A-Z])".r
  val replacer: Match ⇒ String = m ⇒ s"${m.group(1)}_${m.group(2)}"

  def camelToSnake(code: String): String = r2.replaceAllIn(r1.replaceAllIn(code, replacer), replacer).toLowerCase
}

abstract class ErrorTag
