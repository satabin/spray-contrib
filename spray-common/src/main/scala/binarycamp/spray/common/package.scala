package binarycamp.spray

import scala.language.implicitConversions

package object common extends FutureResultSupport {
  type ToStringSerializer[T] = Converter[T, String]
  type ToStringOptionSerializer[T] = Converter[T, Option[String]]
  type ParamMapWriter[T] = Converter[T, Map[String, String]]

  type Properties = Map[String, String]

  type ResolvedMessage = Either[MessageResolutionError, String]
}
