package binarycamp.spray.common

import spray.routing.Rejection

case class ErrorRejection(error: Error) extends Rejection
