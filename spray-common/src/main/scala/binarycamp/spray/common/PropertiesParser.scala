package binarycamp.spray.common

import org.parboiled2._

private[common] class PropertiesParser(val input: ParserInput) extends Parser with StringBuilding {
  import CharPredicate.HexDigit

  def Properties: Rule1[Properties] = rule {
    Empty ~ zeroOrMore(KeyValue ~ Empty) ~ EOI ~> ((fields: Seq[(String, String)]) ⇒ Map(fields: _*))
  }

  def Empty = rule { WhiteSpaceOrLineBreak ~ zeroOrMore(Comment ~ WhiteSpaceOrLineBreak) }

  def WhiteSpaceOrLineBreak = rule { zeroOrMore(WhiteSpaceOrLineBreakChar) }

  def Comment = rule { CommentChar ~ zeroOrMore(!LineBreakChar ~ ANY) ~ EOL }

  def EOL = rule { '\n' | '\r' ~ optional('\n') | &(EOI) }

  def KeyValue = rule { Key ~ optional(Separator) ~ Value ~ EOL ~> ((k, v) ⇒ k -> v) }

  def Key = rule { clearSB() ~ oneOrMore(KeyChar) ~ WhiteSpace ~ push(sb.toString) }

  def KeyChar = rule { NormalKeyChar | '\\' ~ (LineContinuation | EscapedChar) }

  def NormalKeyChar = rule { !KeyReservedChar ~ ANY ~ appendSB() }

  def EscapedChar = rule {
    't' ~ appendSB('\t') |
      'f' ~ appendSB('\f') |
      'n' ~ appendSB('\n') |
      'r' ~ appendSB('\r') |
      Unicode ~> { code ⇒ sb.append(code.toChar); () } |
      ANY ~ appendSB()
  }

  def Unicode = rule { 'u' ~ capture(4.times(HexDigit)) ~> (Integer.parseInt(_, 16)) }

  def LineContinuation = rule { EOL ~ zeroOrMore(WhiteSpaceChar) }

  def WhiteSpace = rule { zeroOrMore(WhiteSpaceChar | '\\' ~ EOL) }

  def Separator = rule { SeparatorChar ~ WhiteSpace }

  def Value = rule { clearSB() ~ zeroOrMore(ValueChar) ~ push(sb.toString) }

  def ValueChar = rule { NormalValueChar | '\\' ~ (LineContinuation | EscapedChar) }

  def NormalValueChar = rule { !ValueReservedChar ~ ANY ~ appendSB() }

  val WhiteSpaceChar = CharPredicate(" \t\f")

  val LineBreakChar = CharPredicate("\n\r")

  val WhiteSpaceOrLineBreakChar = WhiteSpaceChar ++ LineBreakChar

  val CommentChar = CharPredicate("#!")

  val SeparatorChar = CharPredicate(":=")

  val KeyReservedChar = WhiteSpaceChar ++ LineBreakChar ++ SeparatorChar ++ '\\'

  val ValueReservedChar = LineBreakChar ++ '\\'
}
