package binarycamp.spray.common

import scala.concurrent.{ ExecutionContext, Future }
import scala.language.implicitConversions
import spray.util._

final class FutureResult[R](val future: Future[Result[R]]) {
  def mapResult[S](f: R ⇒ S)(implicit ec: ExecutionContext): Future[Either[Error, S]] = future.map(_.toEither.map(f))
}

trait FutureResultSupport {
  implicit def mapResult[R](future: Future[Result[R]]): FutureResult[R] = new FutureResult[R](future)
}

object FutureResultSupport extends FutureResultSupport
