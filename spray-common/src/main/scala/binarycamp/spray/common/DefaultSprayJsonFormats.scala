package binarycamp.spray.common

import spray.http.{ IllegalUriException, Uri }
import spray.json._

trait DefaultSprayJsonFormats extends DefaultJsonProtocol {
  implicit object UriJsonFormat extends JsonFormat[Uri] {
    override def write(uri: Uri): JsValue = JsString(uri.toString())

    override def read(json: JsValue): Uri = json match {
      case JsString(s) ⇒ try Uri(s) catch { case e: IllegalUriException ⇒ deserializationError(s"$s is not a valid URI", e) }
      case _           ⇒ deserializationError(s"Expected Uri encoded as a JsString, but got ${json.compactPrint}")
    }
  }

  protected def field[T](name: String, value: T, rest: List[JsField] = Nil)(implicit w: JsonWriter[T]): List[JsField] =
    w match {
      case _: OptionFormat[_] if value == None ⇒ Nil
      case _                                   ⇒ (name -> w.write(value)) :: rest
    }
}
