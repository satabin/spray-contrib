package binarycamp.spray.common

import java.util.Locale

trait MessageSource {
  def apply(code: String, args: Seq[Any] = Nil, locales: Seq[Locale] = Nil): ResolvedMessage
}
